import com.design_pattern.adapter.SocketAdapter;
import com.design_pattern.adapter.SocketAdapterImplementation;
import com.design_pattern.adapter.SocketObjectAdapterImplementation;
import com.design_pattern.adapter.Volt;
import com.design_pattern.bridge.*;
import com.design_pattern.builder.Builder;
//import com.design_pattern.builder.Person;
import com.design_pattern.chainOfResponsibility.CEOPurchasePower;
import com.design_pattern.chainOfResponsibility.DirectorPurchasePower;
import com.design_pattern.chainOfResponsibility.ManagerPurchasePower;
import com.design_pattern.chainOfResponsibility.PurchaseRequest;
import com.design_pattern.facade.CPU;
import com.design_pattern.facade.ComputerFacade;
import com.design_pattern.facade.HardDrive;
import com.design_pattern.facade.Memory;
import com.design_pattern.factory.Hamburger;
import com.design_pattern.factory.SimpleHamburgerFactory;
import com.design_pattern.mvc.controller.EmployeeController;
import com.design_pattern.mvc.model.Employee;
import com.design_pattern.mvc.view.EmployeeDashboardView;
import com.design_pattern.prototype.Person;
import com.design_pattern.proxy.Bank;
import com.design_pattern.proxy.ProxyBank;
import com.design_pattern.strategy.PayPalAlgorithm;
import com.design_pattern.strategy.Product;
import com.design_pattern.strategy.ShoppingCart;
import com.design_pattern.strategy.creditCardAlgorithm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class application {
    public static void main(String[] args){
        System.out.println("Hello, World!");

        /*
        //strategy

        ShoppingCart cart = new ShoppingCart();
        Product pant = new Product("E21", 45);
        Product shirt = new Product("E31", 15);

        cart.addProduct(pant);
        cart.addProduct(shirt);
        cart.pay(new creditCardAlgorithm("Rocky","12431"));
        cart.pay(new PayPalAlgorithm("Mr.X", "123"));
        */

        /*
        //Singleton

        Singleton singletonOne = Singleton.getInstance();
        Singleton singletonTwo = Singleton.getInstance();
        singletonOne.setName("Test-Singleton");
        System.out.println("singletonOne : "+singletonOne);
        System.out.println("singletonTwo : "+singletonTwo);
        System.out.println(singletonOne.getName());
        System.out.println("" + p);
        */

        /*
        //syncronized Singleton

        Person p = new Person();
        SyncronizeSingleton singletonOne = SyncronizeSingleton.getInstance();
        SyncronizeSingleton singletonTwo = SyncronizeSingleton.getInstance();
        singletonOne.setName("Test-Singleton");
        System.out.println("synchronize singletonOne : "+singletonOne);
        System.out.println("synchronize singletonTwo : "+singletonTwo);
        System.out.println("" + p);
        */

        /*
        //EagerSingleton

        EagerSingleton singletonOne = EagerSingleton.getInstance();
        EagerSingleton singletonTwo = EagerSingleton.getInstance();
        System.out.println("Eager singletonOne : "+singletonOne);
        System.out.println("Eager singletonTwo : "+singletonTwo);
        */

        /*
        //DoubleCheckLockingSingleton

        DoubleCheckLockingSingleton singletonOne = DoubleCheckLockingSingleton.getInstance();
        DoubleCheckLockingSingleton singletonTwo = DoubleCheckLockingSingleton.getInstance();
        System.out.println("Double check locking singletonOne : "+singletonOne);
        System.out.println("Double check locking singletonTwo : "+singletonTwo);
         */

        /*
        factory

        SimpleHamburgerFactory simpleHamburgerFactory = new SimpleHamburgerFactory();
        Hamburger cheese = simpleHamburgerFactory.createHamburger("cheese");
        cheese.prepare();
        cheese.cook();
        cheese.box();

        Hamburger greek = simpleHamburgerFactory.createHamburger("greek");
        greek.prepare();
        greek.cook();
        greek.box();
        */

        /*
          facade

        CPU cpu = new CPU();
        Memory memory = new Memory();
        HardDrive hardDrive = new HardDrive();

        ComputerFacade computerFacade = new ComputerFacade(cpu,memory,hardDrive);
        computerFacade.start();

        */

        // Builder
        /*
        Person oldPerson = new Builder("James", "Bond")
                .phone("007")
                .address("London")
                .age(45)
                .build();
        System.out.println(oldPerson);

         */


        //chain Of Responsibility
        /*

        CEOPurchasePower ceoPurchasePower = new CEOPurchasePower();
        DirectorPurchasePower directorPurchasePower = new DirectorPurchasePower();
        ManagerPurchasePower managerPurchasePower = new ManagerPurchasePower();

        ceoPurchasePower.setSuccessor(directorPurchasePower);
        directorPurchasePower.setSuccessor(ceoPurchasePower);
        managerPurchasePower.setSuccessor(directorPurchasePower);

        while (true) {
            System.out.println("Enter the amount to check who should approve your budget:");
            System.out.print(">>");

            try {
                double d = Double.parseDouble(new BufferedReader(new InputStreamReader(System.in)).readLine());
                managerPurchasePower.processRequest(new PurchaseRequest(d, "By Stuff"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        */

        /*
        //bridge
        Vehicle bmw = new Car(new Make(), new Assemble());
        bmw.manufacture();

        Vehicle bmx = new Bike(new Make(), new Assemble());
        bmx.manufacture();

         */
         //prototype
        /*
        Person person = new Person("James", 45);
        System.out.println("Person 1: " + person);

        Person secondPerson = (Person)person.clone();
        System.out.println("Person copy: " + secondPerson);

        System.out.println(System.identityHashCode(person) + " \n"
                + System.identityHashCode(secondPerson));

         */

         //MVC

        /*

        Employee employee = retrieveEmployeeFromServer();
        //Create our view to which we'll write our employee information into
        EmployeeDashboardView view = new EmployeeDashboardView();
        //Create our controller
        EmployeeController controller = new EmployeeController(employee, view);

//      controller.setEmployee(employee.getFirstName(), employee.getLastName());
        controller.updateDashboardView();

         */

        //adapter
        /*
        testingObjectAdapter();
        testingClassAdapter();

         */

        Bank bank = new ProxyBank();

        try {
            bank.withdrawMoney("Paulo");
            bank.withdrawMoney("me@me");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    //mvc
    /*
    public static Employee retrieveEmployeeFromServer() {
        Employee employee = new Employee();
        employee.setSsNumber("32765523");
        employee.setFirstName("James");
        employee.setLastName("Bond");
        employee.setSalary(125000);

        return employee;
    }
     */

    //adapter
   /*
    private static  void testingClassAdapter() {
        SocketAdapter socketAdapter = new SocketAdapterImplementation();
        Volt v3 = getVolt(socketAdapter, 3);
        Volt v12 = getVolt(socketAdapter, 12);
        Volt v120 = getVolt(socketAdapter, 120);

        System.out.println(" V3 volts is using Class Adapter " + v3.getVolts());
        System.out.println(" V12 volts is using Class Adapter " + v12.getVolts());
        System.out.println(" V120 volts is using Class Adapter " + v120.getVolts());


    }
    private static void testingObjectAdapter() {
        SocketAdapter socketAdapter = new SocketObjectAdapterImplementation();
        Volt v3 = getVolt(socketAdapter, 3);
        Volt v12 = getVolt(socketAdapter, 12);
        Volt v120 = getVolt(socketAdapter, 120);

        System.out.println(" V3 volts is using Object Adapter " + v3.getVolts());
        System.out.println(" V12 volts is using Object Adapter " + v12.getVolts());
        System.out.println(" V120 volts is using Object Adapter " + v120.getVolts());

    }


    private static Volt getVolt(SocketAdapter socketAdapter, int i) {
        switch (i) {
            case 3: return socketAdapter.get3Volts();
            case 12: return  socketAdapter.get12Volts();
            case 120: return socketAdapter.get120Volts();
            default: return socketAdapter.get120Volts();
        }
    }

    */




}
