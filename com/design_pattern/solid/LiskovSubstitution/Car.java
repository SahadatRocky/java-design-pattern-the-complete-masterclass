package com.design_pattern.solid.LiskovSubstitution;

public interface Car {
    void turnOnEngine();
    void accelerate();
}
