package com.design_pattern.solid.LiskovSubstitution;

public class Engine {

    void on(){
        System.out.println("Engine on");
    }

    void powerOn(Integer power){
        System.out.println("Engine power on" + power);
    }
}
