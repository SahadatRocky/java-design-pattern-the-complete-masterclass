package com.design_pattern.solid.LiskovSubstitution;

public class MotorCar implements Car {

    private Engine engine;

    public MotorCar(Engine engine) {
        this.engine = engine;
    }
//Constructors, getters + setters

    public void turnOnEngine() {
        //turn on the engine!
        this.engine.on();
    }

    public void accelerate() {
        //move forward!
        this.engine.powerOn(1000);
    }
}