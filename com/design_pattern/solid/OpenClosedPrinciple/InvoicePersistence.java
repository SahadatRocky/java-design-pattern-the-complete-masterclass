package com.design_pattern.solid.OpenClosedPrinciple;

import com.design_pattern.solid.SingleResponsibilityPrinciple.Invoice;

public interface InvoicePersistence {
    public void save(Invoice invoice);
}
