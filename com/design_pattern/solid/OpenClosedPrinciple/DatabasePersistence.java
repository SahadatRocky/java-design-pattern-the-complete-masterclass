package com.design_pattern.solid.OpenClosedPrinciple;

import com.design_pattern.solid.SingleResponsibilityPrinciple.Invoice;

public class DatabasePersistence implements InvoicePersistence {

    @Override
    public void save(Invoice invoice) {
        // Save to DB
    }
}