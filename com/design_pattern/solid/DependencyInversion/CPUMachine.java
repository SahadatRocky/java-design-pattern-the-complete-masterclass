package com.design_pattern.solid.DependencyInversion;

public class CPUMachine {
    private final StandardKeyboard keyboard;
    private final Monitor monitor;

    public CPUMachine(StandardKeyboard keyboard, Monitor monitor) {
        this.keyboard = keyboard;
        this.monitor = monitor;
    }
}
