package com.design_pattern.solid.InterfaceSegregation;

public interface BearFeeder {
    void feedTheBear();
}
