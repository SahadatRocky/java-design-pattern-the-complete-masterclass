package com.design_pattern.solid.InterfaceSegregation;

public interface BearCleaner {
    void washTheBear();
}
