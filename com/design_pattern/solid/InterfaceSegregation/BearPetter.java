package com.design_pattern.solid.InterfaceSegregation;

public interface BearPetter {
    void petTheBear();
}
