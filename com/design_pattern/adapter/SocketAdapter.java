package com.design_pattern.adapter;

public interface SocketAdapter {
    Volt get120Volts();

    Volt get12Volts();

    Volt get3Volts();

    Volt get1Volt();
}
