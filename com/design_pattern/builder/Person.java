package com.design_pattern.builder;

public interface Person {

   public String getFirstName();
   public String getLastName();
   public String getPhoneNumber();
   public int getAge();
   public String getAddress();
}
