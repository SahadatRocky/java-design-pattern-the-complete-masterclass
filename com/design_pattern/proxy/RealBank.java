package com.design_pattern.proxy;

public class RealBank implements Bank {
        @Override
        public void withdrawMoney(String clientName) {
            System.out.println(clientName + " is withdrawing from the ATM....");
        }
}
