package com.design_pattern.facade;

import java.math.BigInteger;

public class Memory {
    public void load(long position, byte[] data){
        System.out.println("added item to memory  "+ new BigInteger(data).intValue() + " and position " + position);
    }
}
