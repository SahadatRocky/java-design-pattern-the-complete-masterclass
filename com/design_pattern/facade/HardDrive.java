package com.design_pattern.facade;

public class HardDrive {
    public byte[] read(Integer Iba, Integer size){
        byte ib = Iba.byteValue();
        byte s = size.byteValue();
        return new byte[]{ib,s};
    }
}
