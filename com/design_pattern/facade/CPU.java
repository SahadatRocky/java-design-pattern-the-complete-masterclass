package com.design_pattern.facade;

public class CPU {

    public void freeze(){
        System.out.println("Computer freezing ...");
    }

    public void jump(long position){
        System.out.println("jumping to ..."+ position);
    }

    public void execute(){
        System.out.println("Computer executing commands...");
    }

}
