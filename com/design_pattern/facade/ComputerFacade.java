package com.design_pattern.facade;

public class ComputerFacade {
    private CPU processor;
    private Memory ram;
    private HardDrive hd;

    public ComputerFacade(CPU processor, Memory ram, HardDrive hd) {
        this.processor = processor;
        this.ram = ram;
        this.hd = hd;
    }

    public void start(){
        this.processor.freeze();
        this.ram.load(132, this.hd.read(3456,89));
        this.processor.jump(132);
        this.processor.execute();
    }
}
