package com.design_pattern.strategy;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {

    List<Product> productList;

    public ShoppingCart(){
        this.productList = new ArrayList<>();
    }

    public void addProduct(Product product){
        this.productList.add(product);
    }

    public void removeProduct(Product product){
        this.productList.remove(product);
    }

    public int calculateTotal(){
        int sum = 0;
        for(Product p : productList){
            sum += p.getPrice();
        }
        return sum;
    }

    public void pay(Payment payment){
        int amount = calculateTotal();
        payment.pay(amount);
    }
}
