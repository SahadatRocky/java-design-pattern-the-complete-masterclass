package com.design_pattern.strategy;

public class PayPalAlgorithm implements Payment{
    private String name;
    private String password;

    public PayPalAlgorithm(String name, String password) {
        this.name = name;
        this.password = password;
    }

    @Override
    public void pay(int amount) {
        System.out.println(amount +" paid with paypal");
    }
}
