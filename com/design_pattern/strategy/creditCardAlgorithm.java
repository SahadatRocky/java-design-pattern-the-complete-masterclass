package com.design_pattern.strategy;

public class creditCardAlgorithm implements Payment{

    private String name;
    private String cardNumber;

    public creditCardAlgorithm(String name, String cardNumber) {
        this.name = name;
        this.cardNumber = cardNumber;
    }

    @Override
    public void pay(int amount) {
        System.out.println(amount + "paid with card");
    }
}
