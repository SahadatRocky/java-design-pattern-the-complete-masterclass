package com.design_pattern.factory;

public class CheeseBurger extends Hamburger{
    public CheeseBurger() {
        super("cheessy","mircy","cheesy buns");

    }
    @Override
    public void prepare() {
       super.prepare();
    }

    @Override
    public void cook() {
        super.cook();
    }

    @Override
    public void box() {
        super.box();
    }

    @Override
    public String getName() {
        return super.getName();
    }


}
