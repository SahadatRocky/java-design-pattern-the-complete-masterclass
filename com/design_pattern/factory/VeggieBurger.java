package com.design_pattern.factory;

public class VeggieBurger extends Hamburger{
    public VeggieBurger() {
        super("Veggie","soucey Veggie","Veggie buns");
    }

    @Override
    public void prepare() {
       super.prepare();
    }

    @Override
    public void cook() {
        super.cook();
    }

    @Override
    public void box() {
        super.box();
    }

    @Override
    public String getName() {
        return super.getName();
    }
}
