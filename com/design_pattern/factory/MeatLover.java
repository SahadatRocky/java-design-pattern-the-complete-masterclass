package com.design_pattern.factory;

public class MeatLover extends Hamburger{

    public MeatLover() {
        super("Meaty","Heat spicy","Meat buns");
    }

    @Override
    public void prepare() {
        super.prepare();
    }

    @Override
    public void cook() {
       super.cook();
    }

    @Override
    public void box() {
        super.box();
    }

    @Override
    public String getName() {
        return super.getName();
    }
}
