package com.design_pattern.factory;

public interface HamburgerStore {

        void prepare();
        void cook();
        void box();
}
