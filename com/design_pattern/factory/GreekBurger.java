package com.design_pattern.factory;

public class GreekBurger extends Hamburger{

    public GreekBurger() {
        super("Greeky","hot and spicy","Greek buns");
    }

    @Override
    public void prepare() {
        super.prepare();
    }

    @Override
    public void cook() {
        super.cook();
    }

    @Override
    public void box() {
       super.box();
    }

    @Override
    public String getName() {
        return super.getName();
    }
}
