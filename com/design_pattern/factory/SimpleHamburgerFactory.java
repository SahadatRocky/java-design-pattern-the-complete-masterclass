package com.design_pattern.factory;

public class SimpleHamburgerFactory {

    public Hamburger createHamburger(String type){
         Hamburger hamburger = null;
         if(type.equals(("cheese"))){
             hamburger = new CheeseBurger();
         }
         else if(type.equals("greek")){
             hamburger = new GreekBurger();
         }
         else if(type.equals(("veggie"))){
             hamburger = new VeggieBurger();
         }
         else if(type.equals("meatLover")){
             hamburger = new MeatLover();
         }
         return hamburger;
    }
}
