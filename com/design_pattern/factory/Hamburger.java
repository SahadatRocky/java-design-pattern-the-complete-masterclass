package com.design_pattern.factory;

public class Hamburger implements HamburgerStore{

    private String name;
    private  String sauce;
    private  String buns;

    public Hamburger(String name, String sauce, String buns) {
        this.name = name;
        this.sauce = sauce;
        this.buns = buns;
    }

    public void prepare(){
        System.out.println("Preparing " + name);
        System.out.println("Adding sauce..." + sauce);
        System.out.println("Adding buns ..." + buns);


    }
    public void cook(){
        System.out.println(name +"Cooking...");
    }
    public void box(){
        System.out.println(name +"Boxing...");
    }

    public String getName() {
        return name;
    }


}
