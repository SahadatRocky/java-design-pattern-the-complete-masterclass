package com.design_pattern.bridge;

public interface WorkShop {
    void make();
}
