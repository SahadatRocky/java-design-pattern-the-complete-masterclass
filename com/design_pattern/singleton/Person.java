package com.design_pattern.singleton;

public class Person {
    public Person() {
        System.out.println("Creating a person");
    }
}
