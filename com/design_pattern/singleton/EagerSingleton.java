package com.design_pattern.singleton;

public class EagerSingleton {

    //Eager creating SingleTon class
    private static EagerSingleton uniqueInstance = new EagerSingleton();

    private EagerSingleton(){

    }

    public static EagerSingleton getInstance(){

        return uniqueInstance;
    }
}
