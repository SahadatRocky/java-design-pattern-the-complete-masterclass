package com.design_pattern.singleton;

public class SyncronizeSingleton {

    private static SyncronizeSingleton uniqueInstance;
    private String name;
    private SyncronizeSingleton() {
    }

    public static synchronized SyncronizeSingleton getInstance(){
        if(uniqueInstance == null){
            uniqueInstance = new SyncronizeSingleton();
        }
        return uniqueInstance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
