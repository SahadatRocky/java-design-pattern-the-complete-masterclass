package com.design_pattern.singleton;

public class Singleton {

    private static Singleton uniqueInstance;
    private String name;
    private Singleton() {
    }

    public static Singleton getInstance(){
        if(uniqueInstance == null){
            uniqueInstance = new Singleton();
        }
        return uniqueInstance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
