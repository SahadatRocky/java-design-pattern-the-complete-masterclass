package com.design_pattern.prototype;

public interface Animal extends Cloneable  {
    Animal clone();
}
